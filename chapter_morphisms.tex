\chapter{Equivalence of Games} \label{chap:equivalence}
    It is often useful to know whether or not two games are strategically equivalent, for example to determine when two seemingly different situations have the same strategic structure or if a game is symmetric under any notion of the word. 
    
    \section{Game Bijections}
        We use the definition of a game bijection given by Gabarró \textit{et al.} \cite{IsoComplexity}.
    
        \begin{definition} 
            Let $\Gamma_1 = (N, A, u)$ and $\Gamma_2 = (M, B, v)$ be two games. A \textit{bijection} between $\Gamma_1$ and $\Gamma_2$ consists of a bijection $\pi:N\rightarrow M$ and for each player $i \in N$, a bijection $\tau_i:A_i\rightarrow B_{\pi(i)}$. We denote the set of all bijections between $\Gamma_1$ and $\Gamma_2$ as $\bij(\Gamma_1, \Gamma_2)$.
        \end{definition}
            
        For $g = (\pi; (\tau_i)_{i \in N}) \in \bij(\Gamma_1, \Gamma_2)$, $i \in N$, $s_i \in A_i$ and $s \in A$ we use similar notation to that introduced by Stein \textit{et al.} \cite{NoahXE} for symmetric games by denoting $g.i$ as $\pi(i) \in M$, $g.s_i$ as $\tau_i(s_i) \in B_{\pi(i)}$, and $g.s$ as $(\tau_{\pi^{-1}(j)}(s_{\pi^{-1}(j)}))_{j \in M} \in B$ giving $(g.s)_{g.i} = \tau_i(s_i)$.
            
        \begin{definition} 
            For $g = (\pi; (\tau_i)_{i \in N}) \in \bij(\Gamma_1, \Gamma_2)$ and $h = (\eta; (\phi_j)_{j \in M}) \in \bij(\Gamma_2, \Gamma_3)$, their \textit{composite}, denoted $h\circ g$, is $(\eta\circ\pi; (\phi_{\pi(i)}\circ\tau_i)_{i \in N}) \in \bij(\Gamma_1, \Gamma_3)$ giving $(h\circ g).s = h.(g.s)$ for each $s \in A$, and the \textit{inverse} of $g$, denoted $g^{-1}$, is $(\pi^{-1}; (\tau^{-1}_{\pi^{-1}(j)})_{j \in M}) \in \bij(\Gamma_2, \Gamma_1)$.
        \end{definition}
            
        Furthermore, each $g \in \bij(\Gamma_1, \Gamma_2)$ induces $v_{g.i}\circ g \in \mathbb{R}^{A_i}$ where $(v_{g.i}\circ g)(s) = v_{g.i}(g.s)$ for all $s \in A$, and each $g \in \bij(\Gamma_1, \Gamma_2)$ and $\sigma \in \Delta(A)$ induces $\sigma\circ g^{-1} \in \Delta(B)$ where $(\sigma\circ g^{-1})(t) = \sigma(g^{-1}.t)$ for each $t \in B$, giving us $(\sigma\circ g^{-1})(g.s) = \sigma(s)$. When convenient we denote $\sigma \circ g^{-1}$ as $g.\sigma$.
        
        \begin{proposition}
            Games and game bijections are a groupoid.
            
            \begin{proof}
                Let $\Gamma_1 = (N, A, u)$, $\Gamma_2 = (M, B, v)$ and $f = (\pi; (\tau_i)_{i \in N}) \in \bij(\Gamma_1, \Gamma_2)$. Then,
                \begin{align*}
                    f \circ \text{id}_{\Gamma_1} &= (\pi; (\tau_i)_{i \in N}) \circ (\text{id}_N; (\text{id}_{A_i})_{i \in N}) \\
                          &= (\pi \circ \text{id}_N; (\tau_i \circ \text{id}_{A_i})_{i \in N}) \\
                          &= (\pi; (\tau_i)_{i \in N}) = f, 
                \end{align*}
                \begin{align*}
                    \text{id}_{\Gamma_2} \circ f &= (\text{id}_M; (\text{id}_{B_j})_{j \in M}) \circ (\pi; (\tau_i)_{i \in N}) \\
                          &= (\text{id}_M \circ \pi; (\text{id}_{B_{\pi(i)}} \circ \tau_i)_{i \in N}) \\
                          &= (\pi; (\tau_i)_{i \in N}) = f, 
                \end{align*}
                \begin{align*}
                    f \circ f^{-1} &= (\pi; (\tau_i)_{i \in N}) \circ (\pi^{-1}; (\tau^{-1}_{\pi^{-1}(j)})_{j \in M}) \\
                        &= (\pi \circ \pi^{-1}; (\tau_{\pi^{-1}(j)} \circ \tau^{-1}_{\pi^{-1}(j)})_{j \in M}) \\
                        &= (\text{id}_M, (\text{id}_{B_j})_{j \in M}) = \text{id}_{\Gamma_2}, \text{ and} 
                \end{align*}
                \begin{align*}
                    f^{-1} \circ f &= (\pi^{-1}; (\tau^{-1}_{\pi^{-1}(j)})_{j \in M}) \circ (\pi; (\tau_i)_{i \in N}) \\
                        &= (\pi^{-1} \circ \pi; (\tau^{-1}_{\pi^{-1}(\pi(i))} \circ \tau_i)_{i \in N}) \\
                        &= (\text{id}_N, (\text{id}_{A_i})_{i \in N}) = \text{id}_{\Gamma_1}.
                \end{align*}
                
                Now let $\Gamma_3 = (P, C, w)$, $\Gamma_4 = (Q, D, x)$,  $g = (\eta; (\phi_j)_{j \in M}) \in \bij(\Gamma_2, \Gamma_3)$, and $h = (\xi ; (\lambda_k)_{k \in P}) \in \bij(\Gamma_3, \Gamma_4)$. Then, 
                \begin{align*}
                    h \circ (g \circ f) &= (\xi ; (\lambda_k)_{k \in P}) \circ ( (\eta; (\phi_j)_{j \in M}) \circ (\pi; (\tau_i)_{i \in N}) )\\
                        &= (\xi ; (\lambda_k)_{k \in P}) \circ (\eta \circ \pi; (\phi_{\pi(i)} \circ \tau_i)_{i \in N}) \\
                        &= (\xi \circ (\eta \circ \pi); (\lambda_{(\eta \circ \pi)(i)} \circ (\phi_{\pi(i)} \circ \tau_i))_{i \in N}) \\
                        &= (\xi \circ \eta \circ \pi; (\lambda_{(\eta \circ \pi)(i)} \circ \phi_{\pi(i)} \circ \tau_i)_{i \in N}) \text{, and}
                \end{align*}
                \begin{align*}
                    (h \circ g) \circ f &= ( (\xi ; (\lambda_k)_{k \in P}) \circ (\eta; (\phi_j)_{j \in M}) ) \circ (\pi; (\tau_i)_{i \in N}) \\
                        &= (\xi \circ \eta; (\lambda_{\eta(j)} \circ \phi_j)_{j \in M}) \circ (\pi; (\tau_i)_{i \in N}) \\
                        &= ((\xi \circ \eta) \circ \pi; ((\lambda_{\eta(\pi(i))} \circ \phi_{\pi(i)}) \circ \tau_i)_{i \in N}) \\
                        &= (\xi \circ \eta \circ \pi; (\lambda_{(\eta \circ \pi)(i)} \circ \phi_{\pi(i)} \circ \tau_i)_{i \in N}).
                \end{align*}
         
            \end{proof}
        \end{proposition}
     
        
    \section {Game Isomorphisms}
        To distinguish whether or not two games have the same structure we need the concept of structure preserving game bijections, called game isomorphisms. A number of possible isomorphisms are possible, depending on how much structure we want preserved.
        
        We begin with the simplest notion of a game isomorphism which was introduced by Nash \cite{NashNCG}, it requires the preservation of players' utility values.
        
        \begin{definition} 
             A bijection $g \in \bij(\Gamma_1, \Gamma_2)$ is a \textit{strict game isomorphism} if $u_i = v_{g.i}\circ g$ for all $i \in N$. We denote the set of strict game isomorphisms between $\Gamma_1$ and $\Gamma_2$ as $\isom(\Gamma_1, \Gamma_2)$.
        \end{definition}

        \begin{example}
            Two games with the same strategic structure.
            
            \begin{center}
                \begin{game}{2}{2}
                            \> $b_1$  \> $b_2$ \\
                    $a_1$   \> $1,8$  \> $2,7$ \\
                    $a_2$   \> $3,6$  \> $4,5$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}
                            \> $d_1$ \> $d_2$ \\
                    $c_1$   \> $6,3$ \> $8,1$ \\
                    $c_2$   \> $5,4$ \> $7,2$ \\
                \end{game} 
            \end{center}
        
            Let $\pi = \bigl(\begin{smallmatrix} 1 & 2 \\ 2 & 1 \end{smallmatrix}\bigr), \tau_1 = \bigl(\begin{smallmatrix} a_1 & a_2 \\ d_2 & d_1 \end{smallmatrix}\bigr)$, and $\tau_2 = \bigl(\begin{smallmatrix} b_1 & b_2 \\ c_1 & c_2 \end{smallmatrix}\bigr)$, then $g = (\pi; \tau_1, \tau_2)$ is a strict game isomorphism. For example 
            \[ u_1(a_1, b_2) = v_{\pi(1)}(\tau_2(b_2), \tau_1(a_1)) = v_2(c_2, d_2). \]
        \end{example}
        
        Our second type of game isomorphism (see Gabarro \textit{et al.} \cite{IsoComplexity}) requires the preservation of players' preferences over the the pure strategy spaces.
        
        \begin{definition} 
            A bijection $g \in \bij(\Gamma_1, \Gamma_2)$ is an \textit{ordinal game isomorphism} if for each $i \in N$ we have $u_i(s) \leq u_i(s')$ if and only if $v_{g.i}(g.s) \leq v_{g.i}(g.s')$ for all $s, s' \in A$. We denote the set of ordinal game isomorphisms between $\Gamma_1$ and $\Gamma_2$ as $\isom_o(\Gamma_1, \Gamma_2)$.
        \end{definition}
    
        Our third type of game isomorphism (see Harsanyi and Selten \cite{HarsanyiSelten}) requires the preservation of players' preferences over the mixed strategy spaces.
        
        \begin{definition} 
            A bijection $g \in \bij(\Gamma_1, \Gamma_2)$ is a \textit{cardinal game isomorphism} if for each $i \in N$ we have $\tilde{u}_i(\sigma) \leq \tilde{u}_i(\sigma')$ if and only if $\tilde{v}_{g.i}(g.\sigma) \leq \tilde{v}_{g.i}(g.\sigma')$ for all $\sigma, \sigma' \in \Delta(A)$. We denote the set of cardinal game isomorphisms as $\isom_c(\Gamma_1, \Gamma_2)$.
        \end{definition}
            
    \section{Properties of Game Isomorphisms} 
        \begin{proposition} 
            Games and strict, ordinal or cardinal game isomorphisms are a groupoid.
            
            \begin{proof}
                We just prove this for ordinal game isomorphisms as the other proofs are similar.
                
                If $g \in \isom_o(\Gamma_1, \Gamma_2)$ then we have $u_i(s) \leq u_i(s')$ if and only if $v_{g.i}(g.s) \leq v_{g.i}(g.s')$, which is equivalent to $u_{g^{-1}.j}(g^{-1}.t) \leq u_{g^{-1}.j}(g^{-1}.t')$ if and only if $v_j(t) \leq v_j(t')$. Therefore $g^{-1} \in \isom_o(\Gamma_2, \Gamma_1)$.
                
                Let $\Gamma_1 = (N, A, u)$, $\Gamma_2 = (M, B, v)$ and $\Gamma_3 = (P, C, w)$ such that there exists $g \in \isom_o(\Gamma_1, \Gamma_2)$ and $h \in \isom_o(\Gamma_2, \Gamma_3)$. 
                
                Then for each $i \in N$ and $s, s' \in A$ we have, $u_i(s) \leq u_i(s')$ if and only if $v_{g.i}(g.s) \leq v_{g.i}(g.s')$, and $v_{g.i}(g.s) \leq v_{g.i}(g.s')$ if and only if $w_{h.(g.i)}(h.(g.s)) \leq w_{h.(g.i)}(h.(g.s'))$.
                
                Therefore $u_i(s) \leq u_i(s')$ if and only if $w_{(h\circ g).i}((h\circ g).s) \leq w_{(h\circ g).i}((h\circ g).s')$, giving $h \circ g \in isom_o(\Gamma_1, \Gamma_3)$.
                
                Our other conditions follow from games and game bijections being a groupoid.
            \end{proof}
        \end{proposition}
            
        \begin{proposition} \label{ordisoprop}
            $g \in \bij(\Gamma_1, \Gamma_2)$ is an ordinal game isomorphism if and only if for each player $i \in N$ there exists a surjective strictly increasing function $\alpha_i \in \mathbb{R}^{\mathbb{R}}$ such that $\alpha_i \circ u_i = v_{g.i}\circ g$.
            
            \begin{proof}
                Suppose there exists $g \in \bij(\Gamma_1, \Gamma_2)$ and a surjective strictly increasing function $\alpha_i \in \mathbb{R}^{\mathbb{R}}$ such that $\alpha_i \circ u_i = v_{g.i}\circ g$. Then for each $i \in N$ and $s, s' \in A$ such that $u_i(s) \leq u_i(s')$ we have,
                \[ (\alpha \circ u_i)(s) = v_{g.i}(g.s) \leq v_{g.i}(g.s') = (\alpha \circ u_i)(s'). \]
                
                Similarly, for each $j \in M$ and $t, t' \in B$ such that $v_j(t) \leq v_j(t')$, we have,
                \[ (\alpha^{-1} \circ v_j)(t) = u_{g^{-1}.j}(g^{-1}.t) \leq u_{g^{-1}.j}(g^{-1}.t') = (\alpha^{-1} \circ v_j)(t'). \]
                
                Conversely, suppose there exists $g \in \isom_o(\Gamma_1, \Gamma_2)$. 
                
                Let $i \in N$ and $\bar{A}_0 = \argmin_{s \in A} u_i(s)$. 
                
                For each $l \in \mathbb{Z}^+$ let $\bar{A}_l = \argmin_{s \in A - \cup^{l-1}_{j=1}\bar{A}_j} u_i(s)$.
                
                Let $k \in \mathbb{N}$ such that $\bar{A}_k$ is non-empty and $\bar{A}_{k+1}$ is empty.
                
                Let $s_0 \in \bar{A}_0$, ..., and $s_k \in \bar{A}_k$. 
                
                Let $\mu_0 = u_i(s_0)$, ..., $\mu_k = u_i(s_k)$, $\nu_0 = v_{g.i}(g.s_0)$, ..., and $\nu_k = v_{g.i}(g.s_k)$.
                
                Define $\alpha_i \in \mathbb{R}^{\mathbb{R}}$ by 
                \[
                    \alpha_i(x) =
                        \begin{cases}
                            \frac{\nu_0}{\mu_0}x  & \text{if } -\infty < x \leq \mu_0 \\
                            \frac{\nu_1 - \nu_0}{\mu_1 - \mu_0}x + \frac{\mu_1\nu_0 - \nu_1\mu_0}{\mu_1 - \mu_0} & \text{if } \mu_0 < x \leq \mu_1 \\
                                 &  \vdots \\
                            \frac{\nu_k - \nu_{k-1}}{\mu_k - \mu_{k-1}}x + \frac{\mu_k\nu_{k-1} - \nu_k\mu_{k-1}}{\mu_k - \mu_{k-1}}  & \text{if } \mu_{k-1} < x \leq \mu_k \\
                            \frac{\nu_k}{\mu_k}x  & \text{if } \mu_k < x < \infty
                        \end{cases}
                \]
                
                Then $\alpha$ is a surjective strictly increasing function with $\alpha_i \circ u_i = v_{g.i}\circ g$.
            \end{proof}
        \end{proposition}
            
        This shows that the players' preferences over the pure strategy spaces are preserved uniquely up to surjective strictly increasing functions. 
        
        We now prove some of the additional properties relating to the pure strategy spaces which are preserved.
            
        \begin{proposition} 
            If $g \in \isom_o(\Gamma_1, \Gamma_2)$ then by \ref{ordisoprop} for each $i \in N$ there exists a surjective strictly increasing function $\alpha_i \in \mathbb{R}^{\mathbb{R}}$ such that $\alpha_i \circ u_i = v_{g.i} \circ g$, and $g$ preserves; 
            
            \begin{enumerate}
                \item The player's pure best response correspondences.
                \item The game's pure best response correspondence.
                \item The pure strategy Nash equilibria.
                \item Dominance of pure strategies over pure strategies.
            \end{enumerate}
            
            \begin{proof}
                \begin{enumerate}
                    \item Let $i \in N$, $s_{-i} \in A_{-i}$ and $s_i \in b_i(s_{-i})$ so that $u_i(s_i, s_{-i}) \leq u_i(s_i', s_{-i})$ for all $s_i' \in A_i$. Then for each $s_i' \in A_i$ we have,
                          \[(\alpha_i \circ u_i)(s_i, s_{-i}) = v_{g.i}(g.(s_i, s_{-i})) \leq v_{g.i}(g.(s_i', s_{-i})) = (\alpha_i \circ u_i)(s_i', s_{-i}).\] 
                          Therefore $g.s_i \in b_{g.i}(g.s_{-i})$.
                        
                          The reverse argument is the same.
                    \item Let $s' \in A$ and $s \in b(s')$ so that $s_i \in b_i(s_{-i}')$ for all $i \in N$. Then $g.s_i \in b_{g.i}(g.s_{-i}')$ for all $i \in N$. Therefore $g.s \in b(g.s')$.
                            
                          The reverse argument is the same.
                    \item Let $s \in A$ be a pure strategy Nash equilibrium. Then $s \in b(s)$ giving us $g.s \in b(g.s)$, making $g.s$ a pure strategy Nash equilibrium.
                            
                          The reverse argument is the same.
                    \item Let $i \in N$ and $s_i, s_i' \in A_i$ such that $u_i(s_i, s_{-i}) \geq u_i(s_i', s_{-i})$ for all $s_{-i} \in A_{-i}$. Then $v_{g.i}(g.(s_i , s_{-i})) \geq v_{g.i}(g.(s_i', s_{-i}))$ for all $s_{-i} \in A_{-i}$. Therefore $g.s_i$ strictly dominates $g.s_i'$.
                            
                          The reverse argument is the same.
                \end{enumerate}
            \end{proof}
        \end{proposition}
        
        Note that the proofs for Propositions \ref{prop:firstnoahproof} and \ref{cardisoprop} were communicated to the author via email by Noah Stein.
        
        \begin{proposition} \label{prop:firstnoahproof}
            Let $\Gamma_1 = (N, A, u)$, $\Gamma_2 = (M, B, v)$, $i \in N$ and $g \in \bij(\Gamma_1, \Gamma_2)$. If there exists a positive linear transformation $\alpha_i \in \mathbb{R}^{\mathbb{R}}$ such that $\alpha_i \circ u_i = v_{g.i} \circ g$, then $\alpha_i \circ \tilde{u}_i = \tilde{v}_{g.i} \circ g$.
            
            \begin{proof}
                Since $\alpha_i$ is a positive linear transformation, there exists $\beta_i \in \mathbb{R}^+$ and $\gamma_i \in \mathbb{R}$ such that for each $s \in A$ we have $\beta_i.u_i(s) + \gamma_i = v_{g.i}(g.s)$. Therefore for each $\sigma \in \Delta(A)$ we have,
            
                \begin{align*} 
                    (\tilde{v}_{g.i}\circ g)(\sigma) = \tilde{v}_{g.i}(\sigma\circ g^{-1}) &= \sum_{t \in B}\sigma\circ g^{-1}(t).v_{g.i}(t) \\
                        &= \sum_{s \in A}\sigma \circ g^{-1}(g.s).v_{g.i}(g.s)   \\
                        &= \sum_{s \in A}\sigma(s).v_{g.i}(g.s)  \\  
                        &= \sum_{s \in A}\sigma(s).(\beta_i.u_i(s) + \gamma_i) \\  
                        &= \beta_i\sum_{s \in A}(\sigma(s).u_i(s)) + \gamma_i\sum_{s \in A}\sigma(s) \\
                        &= \beta_i.\tilde{u}_i(\sigma) + \gamma_i = (\alpha_i \circ \tilde{u}_i)(\sigma)
                \end{align*}
            \end{proof}
        \end{proposition}
    
        \begin{proposition} \label{cardisoprop}
            $g \in \bij(\Gamma_1, \Gamma_2)$ is a cardinal game isomorphism if and only if for each player $i \in N$ there exists a positive linear transformation $\alpha_i:\mathbb{R}\rightarrow\mathbb{R}$ such that $\alpha_i \circ u_i = v_{g.i}\circ g$.
            
            \begin{proof}
                Suppose for each player $i \in N$ there exists a positive linear transformation $\alpha_i \in \mathbb{R}^{\mathbb{R}}$ such that $\alpha_i \circ u_i = v_{g.i}\circ g$. Then for each $i \in N$ and $\sigma, \sigma' \in \Delta(A)$ such that $\tilde{u}_i(\sigma) \leq \tilde{u}_i(\sigma')$ we have,
                \[ \tilde{v}_{g.i}(\sigma \circ g^{-1}) = (\alpha \circ \tilde{u}_i)(\sigma) \leq (\alpha \circ \tilde{u}_i)(\sigma') = \tilde{v}_{g.i}(\sigma' \circ g^{-1}). \]
                
                Similarly, for each $j \in M$ and $\varsigma, \varsigma' \in B$ such that $\tilde{v}_j(\varsigma) \leq \tilde{v}_j(\varsigma')$, we have,
                \[ (\alpha^{-1} \circ \tilde{v}_j)(\varsigma) = \tilde{u}_{g^{-1}.j}(\varsigma \circ g) \leq \tilde{u}_{g^{-1}.j}(\varsigma' \circ g) = (\alpha^{-1} \circ v_j)(\varsigma'). \]
            
                Conversely, suppose $g \in \isom_c(\Gamma_1, \Gamma_2)$ and let $i \in N$, $\underline{s} = \argmin_{s \in A} u_i(s)$, $\bar{s} = \argmax_{s \in A} u_i(s)$. 
                
                Let $\underline{u} = u_i(\underline{s})$, $\bar{u} = u_i(\bar{s})$, $\underline{v} = v_{g.i}(g.\underline{s})$, and $\bar{v} = v_{g.i}(g.\bar{s})$.
                
                Define $\alpha_i \in \mathbb{R}^{\mathbb{R}}$ by
                \[
                    \alpha_i(x) =
                        \begin{cases}
                            x &\text{if } \underline{u} = \bar{u} = 0 \\
                            \frac{\bar{v}}{\bar{u}}x & \text{if } \underline{u} = \bar{u} \neq 0 \\
                            \frac{\bar{v} - \underline{v}}{\bar{u} - \underline{u}}x + \frac{\bar{u}\underline{v} - \bar{v}\underline{u}}{\bar{u} - \underline{u}} & \text {if } \underline{u} \neq \bar{u} \\
                        \end{cases}
                    \text{ for all } x \in \mathbb{R}
                \]
                
                Then $\alpha_i$ is a positive linear transformation with $(\alpha_i \circ u_i)(\underline{s}) = v_{g.i}(g.\underline{s})$ and $(\alpha_i \circ u_i)(\bar{s}) = v_{g.i}(g.\bar{s})$.
                             
                Now let $s \in A$. Since $u_i(\underline{s}) \leq u_i(s) \leq u_i(\bar{s})$ and $\tilde{u}_i$ is continuous, there exists $\sigma \in \Delta(A)$ such that $\sigma(s') = 0$ for all $s' \in A-\{\underline{s}, \bar{s}\}$ and $u_i(s) = \tilde{u}_i(\sigma)$. Therefore,
                \begin{align*}
                    (\alpha_i \circ u_i)(s) &= (\alpha_i \circ \tilde{u}_i)(\sigma) \\
                        &= \sigma(\underline{s})(\alpha_i \circ u_i)(\underline{s}) + \sigma(\bar{s})(\alpha_i \circ u_i)(\bar{s}) \\
                        &= \sigma(\underline{s})v_{g.i}(g.\underline{s}) + \sigma(\bar{s})v_{g.i}(g.\bar{s}) \\
                        &= \tilde{v}_{g.i}(\sigma \circ g^{-1}) \\
                        &= v_{g.i}(g.s)
                \end{align*}
            \end{proof}
        \end{proposition}
        
        This shows that the players' preferences over the mixed strategy spaces are preserved uniquely up to positive linear transformations of their utility functions. 
        
        We now prove some of the additional properties relating to the mixed strategy spaces which are preserved.
            
        \begin{proposition} 
            If $g \in \isom_c(\Gamma_1, \Gamma_2)$ then by \ref{cardisoprop} for each player $i \in N$ there exists $\beta_i \in \mathbb{R}^+$ and $\gamma_i \in \mathbb{R}$ such that $\beta_i.\tilde{u}_i(\sigma) + \gamma_i = \tilde{v}_{g.i}(\sigma \circ g^{-1})$ for all $\sigma \in \Delta(A)$, and $g$ preserves; 
            
            \begin{enumerate}
                \item The player's best response correspondences.
                \item The game's best response correspondence.
                \item The mixed strategy Nash equilibria.
                \item Dominance of mixed strategies over pure strategies.
            \end{enumerate}
            
            \begin{proof}
                \begin{enumerate}
                    \item Let $i \in N$, $\sigma_{-i} \in \Delta(A_{-i})$ and $\sigma_i \in \tilde{b}_i(\sigma_{-i})$ so that $\tilde{u}_i(\sigma_i, \sigma_{-i}) \geq \tilde{u}_i(\sigma_i', \sigma_{-i})$ for all $\sigma_i' \in \Delta(A_i)$.
                            
                          Then $\tilde{v}_{g.i}((\sigma_i, \sigma_{-i}) \circ g^{-1}) \geq \tilde{v}_{g.i}((\sigma_i', \sigma_{-i}) \circ g^{-1})$ for all $\sigma_i' \in \Delta(A_i)$. Therefore $\sigma_i \circ g^{-1} \in \tilde{b}_{g.i}(\sigma_{-i} \circ g^{-1})$.
                            
                          The reverse argument is the same.
                    \item Let $\sigma' \in \Delta(A)$ and $\sigma \in \tilde{b}(\sigma')$, so that $\sigma_i \in \tilde{b}_i(\sigma_{-i}')$ for all $i \in N$. Then $\sigma_i \circ g^{-1} \in \tilde{b}_{g.i}(\sigma_{-i}' \circ g^{-1})$ for all $i \in N$. Therefore $\sigma \circ g^{-1} \in \tilde{b}(\sigma' \circ g^{-1})$.
                            
                          The reverse argument is the same.
                    \item Let $\sigma \in \Delta(A)$ be a Nash equilibrium. Then $\sigma \in \tilde{b}(\sigma)$ giving us $\sigma \circ g^{-1} \in \tilde{b}(\sigma \circ g^{-1})$. Therefore $\sigma \circ g^{-1}$ is a Nash equilibrium.
                            
                          The reverse argument is the same.
                    \item Let $\sigma_i \in \Delta(A_i)$ and $s_i \in A_i$ such that $\tilde{u}_i(\sigma_i, \sigma_{-i}) \geq \tilde{u}_i(s_i, \sigma_{-i})$ for all $\sigma_{-i} \in \Delta(A_{-i})$.
                        
                          Then $\tilde{v}_{g.i}((\sigma_i , \sigma_{-i}) \circ g^{-1}) \geq \tilde{v}_{g.i}((s_i, \sigma_{-i}) \circ g^{-1})$ for all $\sigma_{-i} \in \Delta(A_{-i})$. Therefore  $\sigma \circ g^{-1}$ strictly dominates $g.s$.
                            
                          The reverse argument is the same.
                \end{enumerate}
            \end{proof}
        \end{proposition}
        
    \section{Notions of Equivalence} 
        
        \begin{definition} 
            Let $\Gamma_1$ and $\Gamma_2$ be two games.
            \begin{itemize}
                \item $\Gamma_1$ and $\Gamma_2$ are \textit{strictly equivalent} if $\isom(\Gamma_1, \Gamma_2)$ is non-empty, which we denote as $\Gamma_1 \cong \Gamma_2$,
                \item $\Gamma_1$ and $\Gamma_2$ are \textit{ordinally equivalent} if $\isom_o(\Gamma_1, \Gamma_2)$ is non-empty, which we denote as $\Gamma_1 \cong_o \Gamma_2$, and 
                \item $\Gamma_1$ and $\Gamma_2$ are \textit{cardinally equivalent} if $\isom_c(\Gamma_1, \Gamma_2)$ is non-empty, which we denote as $\Gamma_1 \cong_c \Gamma_2$. 
            \end{itemize}
        \end{definition}
    
        \begin{proposition} 
            $\cong$, $\cong_o$ and $\cong_c$ are equivalence relations.
            
            \begin{proof}
                For each game $\Gamma$ we have $\id_{\Gamma} \in \isom(\Gamma, \Gamma)$, $\id_{\Gamma} \in \isom_o(\Gamma, \Gamma)$ and $\id_{\Gamma} \in \isom_c(\Gamma, \Gamma)$ which gives reflexivity of $\cong$, $\cong_o$ and $\cong_c$.
                
                Our symmetric and transitivity conditions follow from games and each type of game isomorphisms being a groupoid.
            \end{proof}
        \end{proposition}
        
        \begin{definition} 
            Let $\Gamma$ be a game.
            \begin{itemize}
                \item The \textit{strict equivalence class} of $\Gamma$ is the set $\{\Gamma': \Gamma \cong \Gamma'\}$ which we denote as $[\Gamma]$.
                \item The \textit{ordinal equivalence class} of $\Gamma$ is the set $\{\Gamma': \Gamma \cong \Gamma'\}$ which we denote as $[\Gamma]_o$.
                \item The \textit{cardinal equivalence class} of $\Gamma$ is the set $\{\Gamma': \Gamma \cong \Gamma'\}$ which we denote as $[\Gamma]_c$.
            \end{itemize}
        \end{definition}
        
        All games in each equivalence class have the same strategic structure under that notion. We note that for each set of $n$ player games where each player has $d_i$ strategies there are an infinite number of strict and cardinal equivalence classes, and a finite number of ordinal equivalence classes.
        
        Goforth and Robinson \cite{GoforthRobinson} counted 144 ordinal equivalence classes for the two player games where each player has two strategies and their utility function induces a total order over the pure strategy spaces. 
    
        We now show that strict equivalence is a weaker notion of equivalence than cardinal equivalence, and that cardinal equivalence is a weaker notion of equivalnece than ordinal equivalence. 
    
        \begin{proposition} 
            $\isom(\Gamma_1, \Gamma_2) \subseteq \isom_c(\Gamma_1, \Gamma_2) \subseteq \isom_o(\Gamma_1, \Gamma_2)$.
            
            \begin{proof}
                $\isom(\Gamma_1, \Gamma_2) \subseteq \isom_c(\Gamma_1, \Gamma_2)$ since the idenity function $\id_{\mathbb{R}}$ is a positive linear transformation and $\isom_c(\Gamma_1, \Gamma_2) \subseteq \isom_o(\Gamma_1, \Gamma_2)$ since every positive linear transformation is a surjective strictly increasing function.
            \end{proof}
        \end{proposition}
        
        Of course by transitivity of $\subseteq$ this implies that strict equivalence is also a weaker notion of equivalence than cardinal equivalence.
    
        