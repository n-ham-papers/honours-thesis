\chapter{Introduction} 
Game theory provides a mathematical framework for analysing strategic situations involving at least two players. Since the foundational work of von Neumann and Morgenstern \cite{VNM} it has found applications in areas such as artificial intelligence, biology and economics. 

A normal-form game models a situation where the players simultaneously pick their strategy from a set of strategies and each player assigns a utility or payoff value to every possible combination of the strategies.

Most game theory literature is concerned with how a rational player can optimally select their strategy to maximise their utility, given that each of their opponents is attempting to do the same thing. A lot of this analysis is concerned with the classification of equilibrium concepts and proving their existence. We take an excursion away from this pursuit and explore the strategic structure of finite normal-form games.

Analysing the strategic structure of games should be of interest not only to mathematicians but also people working in other disciplines, for things like determining if a game is fair or under what conditions two situations arising from seemingly different contexts could be considered to have the same strategic structure.

In Chapter \ref{chap:background} we familiarise the reader with some notation and common mathematical terms which will be needed throughout the remainder of the thesis. We provide a proof without reference for any results that may not be common knowledge although we claim no originality to these results.

In Chapter \ref{chap:games} we give a complete definition of a game and a brief introduction to several concepts which will play a central role to the strategic structure of games we want to analyse. 

In Chapter \ref{chap:equivalence} we begin by looking at three notions of isomorphisms between games, the structural properties that they preserve and under what conditions they are met. In particular we look at the necessary conditions for the preservation of structure relating to the pure and mixed strategy spaces of two games. We finish the chapter by defining equivalence between games for each of these notions.

In Chapter \ref{chap:symmgames} we move our focus to symmetric games. We begin by reviewing various notions of a game being symmetric and introduce classifications which better capture the different possible types of symmetric structure. We look at the conditions required for these notions to be met, the structural properties that these notions capture, how to identify them, how to construct them and finally look at two famous theorems on the existence of equilibria in symmetric games.