\chapter{Normal-Form Games} \label{chap:games}
    A normal-form game represents a situation where at least two players simultaneously select a strategy from a set of possible strategies. A combination of players' strategies is called a pure strategy profile, and each player has a utility or payoff value specified for every possible pure strategy profile. 
    
    We assume that all games have common knowledge, which is to say that each player knows not only their own strategies and payoffs, but also those of their opponents, knows that each of their opponents know this, ad infinitum. Each player aims to pick their strategy, possibly randomly, to maximise their utility, knowing that each of their opponents are doing the same.
    
    In this chapter we formally define what a normal-form game is and give a brief overview of several concepts which will play a central role in our analysis for the following chapters. For a more thorough coverage of this material see Myerson \cite{Myerson}.
    
    \section{Definition of a Game}
        \begin{definition} 
            A \textit{normal-form game} consists of a set $N$ of at least two players, where each player $i \in N$ has a non-empty set $A_i$ of strategies and a utility or payoff function $u_i:A\rightarrow\mathbb{R}$ where $A = \times_{i \in N}A_i$ is the \textit{pure strategy space} of the game. Elements of $A$ are typically referred to as \textit{pure strategy profiles}. We denote such a game as the triple $\Gamma = (N, A, u)$.
        \end{definition}
            
        A normal-form game is finite if the player and strategy sets are finite, since we only concern ourselves with finite normal-form games we simply refer to them as games. For each $i \in N$ we denote their number of strategies $|A_i|$ as $d_i$.
        
        \begin{proposition}
            For each player $i \in N$, their utility function induces a preference relation $\precsim_i$ over the pure strategy space where $s \precsim_i s'$ if and only if $u_i(s) \leq u_i(s')$ for all $s, s' \in A$.
            
            \begin{proof}
                This follows from $\leq$ being reflexive, transitive and total on $\mathbb{R}$.
            \end{proof}
        \end{proposition}
        
        Although players must select a single strategy when playing a game, they are able to select their strategy using a random process, we call such a strategy a mixed strategy.
        
        \begin{definition} 
            The \textit{mixed strategy space} $\Delta(A_i)$ for player $i$ is the set $\{\sigma_i \in [0,1]^{A_i}: \sum_{s_i \in A_i} \sigma_i(s_i) = 1\}$ of probability distributions over $A_i$. The \textit{mixed strategy space} $\Delta(A)$ for $\Gamma$ is the Cartesian product of the players' mixed strategy spaces $\times_{i \in N} \Delta(A_i)$. Elements of $\Delta(A)$ are typically referred to as \textit{mixed strategy profiles}. For a mixed strategy profile $\sigma = (\sigma_i)_{i \in N} \in \Delta(A)$ and pure strategy $s = (s_i)_{i \in N} \in A$ we denote $\sigma(s)$ as the product $\prod_{i \in N}\sigma_i(s_i)$. 
        \end{definition}
        
        \begin{proposition}
            The mixed strategy space is a convex subset of $\mathbb{R}^{d_1...d_n}$.
            
            \begin{proof}
                Let $\sigma, \sigma' \in \Delta(A)$ and $p \in [0, 1]$. Then 
                \begin{itemize}
                    \item $(p.\sigma_i + (1-p).\sigma_i')(s_i) = p.\sigma_i(s_i) + (1-p).\sigma_i'(s_i) \in [0, 1]$ for all $i \in N$ and $s_i \in A_i$, and
                    \item $\sum_{s_i \in A_i}(p.\sigma_i + (1-p).\sigma_i')(s_i) = p.\sum_{s_i \in A_i}\sigma_i(s_i) + (1-p).\sum_{s_i \in A_i}\sigma_i'(s_i) = 1$ for all $i \in N$.
                \end{itemize}
                
                Therefore $p.\sigma + (1-p).\sigma' \in \Delta(A)$.
            \end{proof}
        \end{proposition}
        
        While we cannot give a definite utility value to a player for a given mixed strategy profile, we can introduce a notion of expected utility by linearly extending the domain of each player's utility function to the mixed strategy space as follows.
        
        \begin{definition} 
            The \textit{expected utility function} for player $i \in N$ is the function $\tilde{u}_i:\Delta(A)\rightarrow\mathbb{R}$ where $\tilde{u}_i(\sigma) = \sum_{s \in A}\sigma(s)u_i(s)$ for all $\sigma \in \Delta(A)$.
        \end{definition}
        
        \begin{proposition}
            For each player $i \in N$, their expected utility function induces a preference relation $\precsim_i$ over the mixed strategy space where $\sigma \precsim_i \sigma'$ if and only if $\tilde{u}_i(\sigma) \leq \tilde{u}_i(\sigma')$ for all $\sigma, \sigma' \in \Delta(A)$.
            
            \begin{proof}
                This follows from $\leq$ being reflexive, transitive and total on $\mathbb{R}$.
            \end{proof}
        \end{proposition}
        
        \begin{definition}
            Let $\sigma, \sigma', \sigma'' \in \Delta(A)$. For each player $i \in N$, their expected utility function is;
            \begin{itemize}
                \item \textit{Continuous} if $\tilde{u}_i(\sigma) \leq \tilde{u}_i(\sigma') \leq \tilde{u}_i(\sigma'')$ implies the existance of $p \in [0,1]$ such that $\tilde{u}_i(p.\sigma + (1-p)\sigma'') = \tilde{u}_i(\sigma')$, and
                \item \textit{Independent} if $\tilde{u}_i(\sigma) \leq \tilde{u}_i(\sigma')$ implies $\tilde{u}_i(p.\sigma + (1-p).\sigma'') \leq \tilde{u}_i(p.\sigma' + (1-p).\sigma'')$ for all $p \in [0,1]$.
            \end{itemize}
        \end{definition}
        
        \begin{proposition}
            For each player $i \in N$, their utility function is continuous and independent.
            
            \begin{proof}
                Suppose $\tilde{u}_i(\sigma) \leq \tilde{u}_i(\sigma') \leq \tilde{u}_i(\sigma'')$. If $\tilde{u}_i(\sigma) = \tilde{u}_i(\sigma'')$ then our continuity condition is satisfied for all $p \in [0,1]$. Now suppose $\tilde{u}_i(\sigma) \neq \tilde{u}_i(\sigma'')$. Then for continuity we need $p \in [0, 1]$ such that,
                \[ \tilde{u}_i(p.\sigma + (1-p).\sigma'') = p.\tilde{u}_i(\sigma) + (1-p).\tilde{u}_i(\sigma'') = \tilde{u}_i(\sigma) \]
                
                Since this is satisfied for $p = \frac{\tilde{u}_i(\sigma') - \tilde{u}_i(\sigma'')}{\tilde{u}_i(\sigma) - \tilde{u}_i(\sigma'')} \in [0,1]$, $\tilde{u}_i$ is continuous.
                
                Now suppose $\tilde{u}_i(\sigma) \leq \tilde{u}_i(\sigma')$. Then for each $p \in [0, 1]$ we have $\tilde{u}_i(p.\sigma + (1-p).\sigma'') = p\tilde{u}_i(\sigma) + (1-p).\tilde{u}_i(\sigma'') \leq p\tilde{u}_i(\sigma') + (1-p).\tilde{u}_i(\sigma'') = \tilde{u}_i(p.\sigma' + (1-p).\sigma'')$.
                
                Therefore $\tilde{u}_i$ is independent.
            \end{proof}
        \end{proposition}
        
        Totality, transitivity, continuity and independence are the assumptions von Neumann and Morgenstern \cite{VNM} require for players' preferences over simple lotteries in order for such an expected utility function to exist in their famous expected utility theorem.
     
    \section{Table Representation of a Game}
        It can be convenient to represent each player's utility values in a single $n$-dimensional table, where each dimension corresponds to the strategy choices of one of the players, and each element of of the table corresponds to a unique pure strategy profile, which contains the utility values for each of the players. To make this clearer we give a couple of examples below.
        
        \begin{example}
            Below is the famous Prisoner's Dilemma game, the row and columns correspond to the strategies of players 1 and 2 respectively.
            \begin{center}
                \begin{game}{2}{2}
                    \> $d$   \> $c$\\
                $d$ \> $2,2$ \> $1,4$\\
                $c$ \> $4,1$ \> $3,3$
                \end{game} 
            \end{center}
            
            \[ A_1 = A_2 = \{d, c\}, A = \{(d,d), (d,c), (c,d), (c,c)\} \]
            Let $s = (d, c) \in A$ and $\sigma = ((0.2, 0.8), (0.5, 0.5)) \in \Delta(A)$, then $u_2(s) = 4$ and 
            \[ \tilde{u}_2(\sigma) = (0.2)(0.5)2 + (0.2)(0.5)4 + (0.8)(0.5)1 + (0.8)(0.5)3 = 2.2 \]
        \end{example}

        \begin{example}
            Below is a $2\times 2 \times 2$ game where each matrix corresponds to one of player 3's strategies, and the row and columns correspond to the strategies of players 1 and 2 respectively.
            \begin{center}
                \begin{game}{2}{2}[$c_1$]
                            \> $b_1$    \> $b_2$ \\
                    $a_1$   \> $1,1,1$  \> $2,3,2$ \\
                    $a_2$   \> $3,2,2$  \> $4,4,5$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}[$c_2$]
                            \> $b_1$     \> $b_2$ \\
                    $a_1$   \> $2,2,3$ \> $5,4,4$ \\
                    $a_2$   \> $4,5,4$ \> $6,6,6$
                \end{game}
            \end{center}
            
            \[ A_1 = \{a_1, a_2\}, A_2 = \{b_1, b_2\}, A_3 = \{c_1, c_2\} \]
            Let $s = (a_2, b_1, c_2) \in A$, then $u_2(s) = 5$.
        \end{example}
        
    \section{Dominant Strategies}
        While there is no obvious way for players to give a preference ordering of their strategy set, we can introduce a weaker ordering notion with the idea of a player's strategy dominating another one. 
        
        \begin{definition} 
            Let $s_i, s_i' \in A_i$. $s_i$ \textit{strictly dominates} $s_i'$ if $u_i(s_i, s_{-i}) > u_i(s_i', s_{-i})$ for all $s_{-i} \in A_{-i}$, which we denote as $s_i \succ_i s_i'$.
        \end{definition}
        
        \begin{definition} 
            Let $s_i \in A_i$ and $\sigma_i \in \Delta(A_i)$. $\sigma_i$ \textit{strictly dominates} $s_i$ if $u_i(\sigma_i, \sigma_{-i}) > u_i(s_i, \sigma_{-i})$ for all $\sigma_{-i} \in \Delta(A_{-i})$, which we denote as $\sigma_i \succ_i s_i$.
        \end{definition}
        
    \section{Best Response Correspondences}      
        A strategy $s_i \in A_i$ for player $i \in N$ is a \textit{pure best response} to $s_{-i} \in A_{-i}$ if $u_i(s_i, s_{-i}) \geq u_i(s_i', s_{-i})$ for all $s_i' \in A_i$. For each player $i$ and each strategy $s_{-i} \in A_{-i}$, player $i \in N$ has a non-empty set of best responses. 
    
        \begin{definition} 
            Player $i$'s \textit{pure best response correspondence} is the correspondence $b_i:A_{-i}\rightarrow \mathcal{P}(A_i)^*$ where $b_i(s_{-i}) = \argmax_{s_i \in A_i} u_i(s_i, s_{-i})$ for all $s_{-i} \in A_{-i}$. The game's \textit{pure best response correspondence} is the correspondence $b_{\Gamma}:A\rightarrow\mathcal{P}(A)^*$ where $b(s) = \{s' \in A: s'_i \in b_i(s_{-i}) \forall i \in N\}$ for all $s \in A$.
        \end{definition}
        
        A mixed strategy $\sigma_i \in \Delta(A_i)$ for player $i \in N$ is a best response to $\sigma_{-i} \in \Delta(A_{-i})$ if $\tilde{u}_i(\sigma_i, \sigma_{-i}) \geq \tilde{u}_i(\sigma_i', \sigma_{-i})$ for all $\sigma_i' \in \Delta(A_i)$. For each player $i$ and each strategy $\sigma_{-i} \in \Delta(A_{-i})$, player $i \in N$ has a non-empty set of best responses. 
                    
        \begin{definition} 
            Player $i$'s \textit{best response correspondence} is the correspondence $\tilde{b}_i:\Delta(A_{-i})\rightarrow\mathcal{P}(\Delta(A_i))^*$ where for each $\sigma_{-i} \in \Delta(A_{-i})$, $\tilde{b}_i(\sigma_{-i}) = \argmax_{\sigma_i \in \Delta(A_i)} \tilde{u}_i(\sigma_i, \sigma_{-i})$. The game's \textit{best response correspondence} is the correspondence $\tilde{b}:\Delta(A)\rightarrow\mathcal{P}(\Delta(A))^*$ where for each $\sigma \in \Delta(A)$, $\tilde{b}(\sigma) = \{\sigma' \in \Delta(A): \sigma'_i \in \tilde{b}_i(\sigma_{-i}) \ \forall \ i \in N\}$.
        \end{definition}
        
    \section{Nash Equilibria}
        The most central equilibrium concept in game theory is that of Nash equilibria. They represent strategy profiles where no player can deviate from their specified strategy to increase their payoff, given the strategies being played by their opponents.
        
        \begin{definition}
            A strategy profile $s \in A$ is a \textit{pure strategy Nash equilibrium} if for each player $i$, there does not exist an alternative strategy $s_i' \in A_i$ such that $u_i(s_i', s_{-i}) > u_i(s_i, s_{-i})$. 
        \end{definition}
        
        \begin{definition}
            A strategy profile $\sigma \in \Delta(A)$ is a \textit{Nash equilibrium} if for each player $i \in N$, there does not exist an alternative strategy $\sigma_i' \in \Delta(A_i)$ such that $\tilde{u}_i(\sigma_i', \sigma_{-i}) > \tilde{u}_i(\sigma_i, \sigma_{-i})$. 
        \end{definition}
        
        It is fairly straightforward to see that a mixed strategy profile $\sigma \in \Delta(A)$ is a Nash equilibrium if and only if it is a fixed point of the best response correspondence $\tilde{b}$.
        
        We now state Nash's \cite{NashNCG} famous theorem without proof which gives the eixstence of a Nash equilibria. 
        
        \begin{theorem} 
            Every game has a Nash equilibrium.
        \end{theorem}
        
        One way to prove this is to show that the mixed strategy space of the game is a non-empty, compact and convex subset of $\mathbb{R}^{d_1...d_n}$, that $\tilde{b}$ has a closed graph and that $\tilde{b}(\sigma)$ is convex for all $\sigma \in \Delta(A)$. Existence then follows from Kakutani's fixed point theorem. 
