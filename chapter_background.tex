\chapter{Background} \label{chap:background}
    We begin by familiarising the reader with some notation and mathematical terms that will be used throughout the remainder of the thesis. We prove some of the results that may not be common knowledge, but we claim no originality to them.
        
    \section{Binary Relations}
        \begin{definition} 
            A \textit{binary relation} $R$ between two sets $A$ and $B$ is a subset of $A\times B$. We say that $a \in A$ is \textit{related} to $b \in B$ if $(a,b) \in R$ and write $a R b$. The $inverse$ of a binary relation $R$ is $\{(b,a): (a,b) \in R\} \subseteq B\times A$ denoted as $R^{-1}$.
        \end{definition}
            
        \begin{properties} 
            A binary relation $R \subseteq A\times B$ is; 
            \begin{itemize}
                \item \textit{injective} if for all $a_1, a_2 \in A$ and $b \in B$, $(a_1, b) \in R$ and $(a_2, b) \in R$ implies $a_1 = a_2$,
                \item \textit{functional} if for all $a \in A$ and $b_1, b_2 \in B$, $(a, b_1), (a, b_2) \in R$ implies $b_1 = b_2$,
                \item \textit{left-total} if for all $a \in A$ there exists $b \in B$ such that $(a,b) \in R$,  
                \item \textit{surjective} if for all $b \in B$ there exists $a \in A$ such that $(a,b) \in R$,
                \item \textit{reflexive} if $B = A$ and $(a, a) \in R$ for all $a \in A$,
                \item \textit{symmetric} if $B = A$ and for all $a_1, a_2 \in A$, $(a_1, a_2) \in R$ implies $(a_2, a_1) \in R$.
                \item \textit{transitive} if $B = A$ and $(a_1, a_2), (a_2, a_3) \in R$ implies $(a_1, a_3) \in R$, and
                \item \textit{total} if $B = A$ and for all $a_1, a_2 \in A$, $(a_1, a_2) \in R$ or $(a_2, a_1) \in R$,
            \end{itemize}
        \end{properties}
            
        \begin{definition} 
            An \textit{equivalence relation} on a set $A$ is a reflexive, symmetric and transitive binary relation $\sim \ \subseteq A\times A$. The relation $\sim$ parititons $A$ into equivalence classes $[a]$ where for each $a \in A$, we define $[a] = \{a' \in A: a \sim a'\}$.
        \end{definition}
        
    \section{Functions and Correspondences}
        \begin{definition} 
            Let $X$ and $Y$ be non-empty sets. A \textit{function} from $X$ to $Y$ is a functional left-total binary relation $f \subseteq X\times Y$, which we denote as $f:X\rightarrow Y$. We denote the set of functions from $X$ to $Y$ as $Y^X$.
        \end{definition}
            
        Let $f:X\rightarrow Y$ be a function. Since $f$ is functional, for each $x \in X$ we can denote by $f(x)$ the unique element in $Y$ such that $(x, f(x)) \in f$. The \textit{image} of $f$ is the set $\{f(x): x \in X\}$ which we denote as $\Image(f)$. If $Y = X$ then the function that maps each element of $X$ to itself acts as an identity under composition, we call this the \textit{identity function} and denote it as $\id_X:X\rightarrow X$.
        
        \begin{definition} 
            A \textit{bijection} is an injective and surjective function. Bijections form a group under composition.
        \end{definition}
    
        \begin{definition} 
            A function $f:\mathbb{R}\rightarrow\mathbb{R}$ is \textit{strictly increasing} if for each $x, y \in \mathbb{R}$ we have $x < y$ if and only if $f(x) < f(y)$.
        \end{definition}
            

        \begin{proposition} 
            Surjective strictly increasing functions form a group under composition.
            
            \begin{proof}
                The identity function $\id_{\mathbb{R}}$ is a surjective increasing function giving the existence of an identity.
            
                Let $f, g:\mathbb{R}\rightarrow\mathbb{R}$ be surjective increasing functions.
                
                If $x, y \in \mathbb{R}$ such that $f(x) = f(y)$ then we must have $x = y$, making $f$ injective. Therefore $f^{-1}$ exists and is also a bijection.
                
                For each $x, y \in \mathbb{R}$ we have $x < y$ if and only if $f(x) < f(y)$, which gives $f^{-1}(x) < f^{-1}(y)$ if and only if $x < y$. Therefore $f^{-1}$ is also a surjective increasing function.
                
                Now $g \circ f$ is surjective and for each $x, y \in \mathbb{R}$ we have $x < y$ if and only if $f(x) < f(y)$ and $f(x) < f(y)$ if and only if $g(f(x)) < g(f(y))$. Therefore $x < y$ if and only if $(g \circ f)(x) < (g \circ f)(y)$ making $g \circ f$ strictly increasing.
            \end{proof}
        \end{proposition}
        
        \begin{definition} 
            A \textit{positive linear transformation} is a function $f:\mathbb{R}\rightarrow\mathbb{R}$ where for some $\alpha \in \mathbb{R}^+$ and $\beta \in \mathbb{R}$, $f(x) = \alpha x + \beta$ for all $x \in \mathbb{R}$.
        \end{definition}
            
        \begin{proposition} 
            Positive linear transformations form a subgroup of the surjective strictly increasing functions under composition.
            
            \begin{proof}
                A positive linear transformation is differentiable and its derivative is positive making it a strictly increasing function.
            
                The identity function which maps real numbers to themselves is a positive linear transformation giving the existence of an identity.
                
                Let $\alpha, \gamma \in \mathbb{R}^+$, $\beta, \delta \in \mathbb{R}$ and $f, g:\mathbb{R}\rightarrow\mathbb{R}$ be positive linear transformations where for each $x \in \mathbb{R}$ we have $f(x) = \alpha x + \beta$ and $g(x) = \gamma x + \delta$.
            
                For each $x \in \mathbb{R}$ we have $f^{-1}(x) = \frac{x - \beta}{\alpha} = \frac{1}{\alpha}x - \frac{\beta}{\alpha}$ and $(g \circ f)(x) = \gamma(\alpha x + \beta) + \delta = \alpha\gamma x + (\beta\gamma + \delta)$ which are both positive linear transformations.
            \end{proof}
        \end{proposition}
    
        \begin{definition} 
            A \textit{correspondence} between two sets $X$ and $Y$ is a left-total binary relation $c \subseteq X\times Y$. For simplicity, we can treat a correspondence $c \subseteq X\times Y$ as a function $c:X\rightarrow \mathcal{P}(Y)^*$ where $\mathcal{P}(Y)^* = \mathcal{P}(Y)-\{\varnothing \}$ and for each $x \in X$ we have $c(x) = \{y \in Y: (x,y) \in c\}$.
        \end{definition}
            
        \begin{definition} 
            A \textit{fixed point} of a correspondence $c:X\rightarrow\mathcal{P}(Y)^*$ is an element $x \in X$ such that $x \in c(x)$.
        \end{definition}
        
        We now state Kakutani's \cite{Kakutani} fixed point theorem without proof which will be referred to when sketching out how to prove the existence of a Nash equilibrium in the Chapter \ref{chap:games}.
        
        \begin{theorem} 
            If $X$ is a non-empty, compact and convex subset of $\mathbb{R}^d$ and $c:X\rightarrow\mathcal{P}(X)^*$ is a correspondence on $X$ with a closed graph and the property that $c(x)$ is convex for all $x \in X$. Then there exists a fixed point $x \in X$ such that $x \in c(x)$.
        \end{theorem}
        
    \section{Preference Relations}
        It is common in economics to require an agent to assign preferences to a set of alternatives $A$ which are captured with the notion of a preference relation.
        
        \begin{definition} 
            A \textit{preference relation} or \textit{total preorder} for an agent over a set $A$ is a reflexive, transitive and total binary relation $\precsim \subseteq A\times A$. If $(a_1, a_2) \in \precsim$ we write $a_1 \precsim a_2$ and say that $a_2$ is \textit{weakly preferred to} or \textit{at least as good as} $a_1$.
        \end{definition}
        
        \begin{definition} 
            Two preference relations $\precsim_1 \subseteq A \times A$ and $\precsim_2 \subseteq B \times B$ are \textit{isomorphic} if there exists a bijection $f:A\rightarrow B$ such that $a_1 \precsim_1 a_2$ if and only if $f(a_1) \precsim_2 f(a_2)$.
        \end{definition}
        
    \section{Relations and Matchings}
	Let $N$ and $\{A_i: i \in N\}$ be non-empty sets. To simplify notation for each $i \in N$ we denote $A_{-i}$ as the set $\times_{j \in N-\{i\}} A_j$, and for $a_i \in A_i$ and $a_{-i} = (a_1, ..., a_{i-1}, a_{i+1}, ...) \in A_{-i}$ we denote $(a_i, a_{-i})$ as $(a_1, ..., a_{i-i}, a_i, a_{i+1}, ...) \in \times_{i \in N} A_i$.
    
    \begin{definition} 
        Let $N$ and $\{A_i: i \in N\}$ be non-empty sets. A \textit{relation} $R$ on  $\{A_i: i \in N\}$ is a subset of $\times_{i \in N} A_i$.
    \end{definition}
        

            
        \begin{properties} 
            A relation $R \subseteq \times_{i \in N} A_i$ is; 
            \begin{itemize}
                \item $i-unique$ if for all $a_i \in A_i$ and $a_{-i}, a_{-i}' \in A_{-i}$, $(a_i, a_{-i}), (a_i, a_{-i}') \in R$ implies $a_{-i} = a_{-i}'$.
                \item $i-total$ if for all $a_i \in A_i$ there exists $a_{-i} \in A_{-i}$ such that $(a_i, a_{-i}) \in R$.
            \end{itemize}
        \end{properties}

        \begin{definition} 
            A \textit{matching} $M$ of $n \geq 2$ sets $A_1, ..., A_n$ is a relation which is $i$-total and $i$-unique for all $1 \leq i \leq n$. 
        \end{definition}
        
        A matching of $A_1, ..., A_n$ requires $|A_1| = ... = |A_n|$ and takes the form:
        \[\{(a_1, ..., a_n) : \text{each } a_i \text{ appears in exactly one $n$-tuple}\}.\]
        
        For $1 \leq i, j \leq n$ a matching $M$ induces a unique bijection $M_{ij}:A_i\rightarrow A_j$ which maps elements $a_i \in A_i$ to the unique element $a_j \in A_j$ such that $a_i$ and $a_j$ are elements of an n-tuple in $M$. $M_{ii}$ is the identity bijection for all $1 \leq i \leq n$ and $M_{kl} \circ (M_{jk} \circ M_{ij}) = (M_{kl} \circ M_{jk}) \circ M_{ij}$ for all $1 \leq i, j, k, l \leq n$.
        
    \section{Groupoids}
        \begin{definition} 
            A \textit{groupoid} consists of
            \begin{enumerate}
                \item A non-empty set of objects $C$, and
                \item For each pair of objects $X, Y \in C$ a set of bijective morphisms from $X$ to $Y$ denoted as $C(X, Y)$.
            \end{enumerate}
            
            Satisfying
            \begin{itemize}
                \item For every object $X \in C$ there exists an element $\id_X \in C(X, X)$, 
                \item For every triple of objects $X, Y, Z \in C$ there exists a composite function $\circ:C(Y, Z)\times C(X, Y)\rightarrow C(X, Z)$, and
                \item For every pair of objects $X, Y \in C$ there exists an inverse function $\inv:C(X, Y)\rightarrow C(Y, X)$.
            \end{itemize}
            
            Also satsifying the additional properties that for each $W, X, Y, X \in C$ and $f \in \bij(W, X), g \in \bij(X, Y)$, and $h \in \bij(Y, Z)$
            \begin{itemize}
                \item $f \circ \id_W = f = \id_X \circ f$,
                \item $h \circ (g \circ f) = (h \circ g) \circ f$, and
                \item $f \circ \inv f = \id_Y$ and $\inv f \circ f = \id_X$.
            \end{itemize}
        \end{definition}
           
        