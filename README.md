Some Structure Properties of Finite Normal-Form Games
=====

author: [Nicholas Ham](https://www.n-ham.com).

A thesis submitted December 2011 in fulfilment of the requirements for the Degree of Bachelor of Science with Honours at the [University of Tasmania](http://www.utas.edu.au).

-----

Enjoy!

Copyright (C) 2018 [Nicholas Ham](https://www.n-ham.com).
