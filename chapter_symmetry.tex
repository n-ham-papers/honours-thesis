\chapter{Symmetric Games} \label{chap:symmgames}
    Symmetric games capture the structural notion of a game being fair. Not only is this useful for classifying games but this structure can also be exploited for computational and theoretical purposes. 
    
    In the simplest sense, we would like a symmetric game to represent the same situation regardless of the player. Under any such definition it is necessary for all players to have the same number of strategies so we make this assumption for the remainder of the chapter. 

    \section {Automorphism Group}
        \begin{definition} 
            An isomorphism $g \in \isom(\Gamma, \Gamma)$ is an $automorphism$ or $symmetry$ of $\Gamma$. We denote the set of $automorphisms$ as $\Aut(\Gamma)$.
        \end{definition}
        
        \begin{example}
            The Prisoner's Dilemma
            \begin{center}
                \begin{game}{2}{2}
                          \> $d$    \> $c$ \\
                    $d$   \> $3,3$  \> $1,4$ \\
                    $c$   \> $4,1$  \> $2,2$
                \end{game} 
            \end{center}
            
            \[ Aut(\Gamma) = \{ \bigl(e ; \bigl(\begin{smallmatrix} d & c \\ d & c \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} d & c \\ d & c \end{smallmatrix}\bigr)\bigr),
                                \bigl((12) ; \bigl(\begin{smallmatrix} d & c \\ d & c \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} d & c \\ d & c \end{smallmatrix}\bigr)\bigr) \}  \]
        \end{example}
            
        \begin{proposition} 
            The automorphisms of a game form a group under composition.
            
            \begin{proof}
                This follows from the fact that games and strict game isomorphisms are a groupoid.
            \end{proof}
        \end{proposition}
        
        \begin{remark}
            Let $M$ be a matching of the strategy sets. For each player permutation $\pi \in S_N$, $M$ induces a game bijection $(\pi; (M_{i\pi(i)})_{i \in N}) \in \bij(\Gamma, \Gamma)$ which we denote as $M_{\pi}$.
        \end{remark}
        
        
    \section {Symmetry Groups}    
        Below we define symmetry groups for games which were introduced by Stein \textit{et al.} \cite{NoahXE}. Symmetry groups make the classification of symmetric games in the next section much clearer.
            
        \begin{definition} 
            A \textit{symmetry group} of a game $\Gamma$ is a subgroup $G$ of the automorphism group $\Aut(\Gamma)$. The \textit{stabiliser subgroup} for player $i$ is the subgroup of automorphisms $\{g \in G: g.i = i\}$ that map player $i$ to itself, which we denote as $G_i$.
        \end{definition}
            
        \begin{properties} 
            Let $G$ be a symmetry group of a game $\Gamma = (N, A, u)$. We say that $G$ is;
            \begin{itemize}
                \item \textit{player transitive} if $G$ acts transitively on $N$, that is for each $i, j \in N$ there exists $g \in G$ such that $g.i = j$,
                \item \textit{player n-transitive} if $G$ acts $n$-transitively on $N$, that is for each $\pi \in S_N$ there exists $g \in G$ such that $g.i = \pi(i)$ for all $i \in N$, and
                \item \textit{strategy trivial} if for each $g \in G_i$, $g.s_i = s_i$ for all $s_i \in A_i$.
            \end{itemize}
        \end{properties}
           
    
    \section {Notions of Symmetry}
        In this section we introduce various notions of a game being symmetric. Von Neumann and Morgenstern \cite{VNM} were the first to consider symmetric games, which they defined as follows. 

        \begin{definition} 
            A game $\Gamma$ is \textit{VNM symmetric} if $A_i = A_j$ for all $i, j \in N$, and for each permutation $\pi \in S_N$,  $u_{\pi(i)}(s_1, ..., s_n) = u_i(s_{\pi(1)}, ..., s_{\pi(n)})$ for all $i \in N$ and $(s_1, ..., s_n) \in A$.
        \end{definition}
        
        \begin{example}
            Two Player VNM Symmetric Game
            \begin{center}
                \begin{game}{2}{2}
                          \> $a$    \> $b$ \\
                    $a$   \> $1,1$  \> $3,2$ \\
                    $b$   \> $2,3$  \> $4,4$
                \end{game} 
            \end{center}
            
            For $\pi = \bigl(\begin{smallmatrix} 1 & 2 \\ 2 & 1 \end{smallmatrix}\bigr) \in S_2$ we have
            \begin{align*}
                u_1(a, a) &= u_2(a, a) = 1   &   u_1(a, b) &= u_2(b, a) = 3 \\
                u_1(b, a) &= u_2(a, b) = 2   &   u_1(b, b) &= u_2(b, b) = 4
            \end{align*} 
        \end{example}
        
        While VNM symmetric games are obviously fair, they do not fully capture the notion of fairness, nor do they classify the possible notions of structural fairness that may be present. We now define the classifications of symmetric games which we think best capture the possible structural notions of fairness.
        
        Nash \cite{NashNCG} provided the most general definition of symmetric game as follows.
        
        \begin{definition} 
            A game $\Gamma$ is \textit{symmetric} if its automorphism group is player transitive.
        \end{definition}
        
        Not only does this drop the requirement that strategy sets be equal, it also drops the assumption that we are able to match up the strategy sets such that the game be automorphic under the induced game bijections or that the game be automorphic under every permutation of the players.
        
        Our next notion of a symmetric game was introduced by Stein \textit{et al.} \cite{NoahXE} which strengthens the conditions for a symmetric game.
        
        \begin{definition} 
            A game $\Gamma$ is \textit{standard symmetric} if there exists a player transitive and strategy trivial symmetry group.
        \end{definition}
        
        All symmetric games presented so far have been standard symmetric, below we provide an example of a symmetric game which is not standard symmetric.
        
        \begin{example}
            Matching Pennies
            \begin{center}
                \begin{game}{2}{2}
                          \> $H$    \> $T$ \\
                    $H$   \> $1,-1$  \> $-1,1$ \\
                    $T$   \> $-1,1$  \> $1,-1$
                \end{game} 
            \end{center}
            
            \begin{align*}
                 \Aut(\Gamma) = \{ &\bigl(e ; \bigl(\begin{smallmatrix} H & T \\ H & T \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} H & T \\ H & T \end{smallmatrix}\bigr)\bigr), 
                                \bigl(e ; \bigl(\begin{smallmatrix} H & T \\ T & H \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} H & T \\ T & H \end{smallmatrix}\bigr)\bigr), \\
                                &\bigl((12) ; \bigl(\begin{smallmatrix} H & T \\ H & T \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} H & T \\ T & H \end{smallmatrix}\bigr)\bigr),
                                \bigl((12) ; \bigl(\begin{smallmatrix} H & T \\ T & H \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} H & T \\ H & T \end{smallmatrix}\bigr)\bigr) \}
            \end{align*}
            Since $\Aut(\Gamma)$ is player transitive, is not strategy trivial and contains no proper subgroups, matching pennies is an example of a symmetric game which is not standard symmetric.
        \end{example}
        
        We now make another stricter definition of a symmetric game.
        
        \begin{definition} 
            A game $\Gamma$ is \textit{fully symmetric} if its automorphism group is player $n$-transitive.
        \end{definition}
        
        Finally, we make the following definitions which partition the set of symmetric games up into different classes based on the kind of structure they contain.
        
        \begin{definition} 
            A game $\Gamma$ is;
            \begin{itemize}
                \item \textit{non-fully non-standard symmetric} if it is symmetric but neither fully or standard symmetric,
                \item \textit{fully non-standard symmetric} if it is fully symmetric and not standard symmetric,
                \item \textit{non-fully standard symmetric} if it is standard symmetric and not fully symmetric, and
                \item \textit{fully standard symmetric} if it is fully symmetric and standard symmetric.
            \end{itemize}
        \end{definition}
        
    \section{Properties of Symmetric Games}
        VNM symmetric games do not have the desirable property that if $\Gamma_1 \cong \Gamma_2$ and $\Gamma_1$ is VNM symmetric then $\Gamma_2$ is also VNM symmetric, which we illustrate with the example below.
        
        \begin{example}
            Consider the two following equivalent games, the first being the prisoners dilemma.
            
            \begin{center}
                \begin{game}{2}{2}[$\Gamma_1$]
                            \> $d$  \> $c$ \\
                    $d$   \> $3,3$  \> $1,4$ \\
                    $c$   \> $4,1$  \> $2,2$
                \end{game}
                \hspace*{10mm} $\cong$ \hspace*{10mm}
                \begin{game}{2}{2}[$\Gamma_2$]
                            \> $a$ \> $b$ \\
                    $a$   \> $1,4$ \> $3,3$ \\
                    $b$   \> $2,2$ \> $4,1$ \\
                \end{game} 
            \end{center}
        
            \[ Isom(\Gamma_1, \Gamma_2) = \{ \bigl(e ; \bigl(\begin{smallmatrix} d & c \\ a & b \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} d & c \\ b & a \end{smallmatrix}\bigr)\bigr),
                                \bigl((12) ; \bigl(\begin{smallmatrix} d & c \\ b & a \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} d & c \\ a & b \end{smallmatrix}\bigr)\bigr) \}  \]
                                
            Clearly $\Gamma_1$ is VNM symmetric and $\Gamma_1 \cong \Gamma_2$, but $\Gamma_2$ is not VNM symmetric.
        \end{example}
       
            
        We now show that standard symmetric games capture the notion that the game is symmetric under some matching of the strategy sets, we note that we use the argument from Stein \textit{et al.} \cite{NoahXE} to show that a player transitive and strategy trivial symmetry group maps between strategy sets in a canonical manner.
        
        \begin{proposition} 
            A game $\Gamma$ is standard symmetric if and only if there exists a matching of the strategy sets such that each game bijection induced from the player permutations in some transitive subgroup of $S_N$ is an automorphism.
            
            \begin{proof}
                Let $\Gamma$ be a standard symmetric game, $G$ any player transitive and strategy trivial subgroup of $Aut(\Gamma)$, and $g, h \in G$ such that $g.i = h.i = j$. 
                
                For each $g, h \in G$ such that $g.i = h.i = j$ we have $h^{-1} \circ g \in G_i$, strategy triviality of $G$ gives $(h^{-1} \circ g).s_i = s_i$ for all $s_i \in A_i$, so we must have $g.s_i = h.s_i$ for all $s_i \in A_i$. Hence $G$ maps $A_i$ to $A_j$ in a canonical way.
                
                This along with player transitivity of $G$ induces a matching of the strategy sets such that each game bijection induced from the player bijections in some transitive subgroup of $S_N$ is an automorphism.
                
                Conversely, let $\Gamma$ be a game, $H$ a transitive subgroup of $S_N$ and $M$ a matching of the strategy sets such that each game bijection induced from the player permutations in $H$ is an automorphism.
                
                Then $\{M_{\pi} \in \Aut(\Gamma): \pi \in H\}$ is a player transitive and strategy trivial subgroup of $\Aut(\Gamma)$.
            \end{proof}
        \end{proposition}
        
        \begin{proposition}
            If $\Gamma$ is standard symmetric so that there exists a matching $M$ of the strategy sets such that each game bijection induced from the player permutations in some transitive subgroup $H$ of $S_N$ is an automorphism, then for each $i, j \in N$ we have $u_i(s) = u_j(s)$ for all $s \in M$.
            
            \begin{proof}
                This follows from $\{M_{\pi} \in \Aut(\Gamma): \pi \in S_N\}$ being a player transitive symmetry group and that $M_{\pi}.s = s$ for all $s \in M$ and $\pi \in S_N$.
            \end{proof}
        \end{proposition}
        
        This shows that in a standard symmetric game all of the players must assign the same utility to each strategy profile in some matching of the pure strategy space.
        
        \begin{proposition}
            If a game has a player $n-transitive$ and strategy trivial symmetry group then it is fully standard symmetric.
            
            \begin{proof}
                This follows directly from the definition of fully and standard symmetric games.
            \end{proof}
        \end{proposition}
    
        It is not entirely clear whether a fully standard symmetric game implies the existence of a player $n$-transitive and strategy trivial symmetry group, although it would be desirable if it did for a couple of reasons discussed below. We have been unable to show this however, so have left it as a conjecture.
        
        \begin{conjecture} 
            A game is fully standard symmetric if and only if it has a player $n$-transitive and strategy trivial symmetry group. (Note: conjecture is false, see the more recent paper `Notions of Symmetry for Finite Strategic-Form Games' by the author).
        \end{conjecture}
        
        Suppose this were not the case, then our neat classifications of symmetric games fall to pieces as it is not intuitively clear by name that a fully symmetric game is not defined as a game which has a player $n$-transitive and strategy trivial symmetry group. 
    
        The lack of the desirable property for VNM symmetric games outlined above is a result of the definition implicitly imposing an order on each player's strategy set. We now look at where VNM symmetric games lie with respect to our other classifications of symmetric games. 
        
        \begin{proposition} 
            A game is VNM symmetric if and only if for each player permutation $\pi \in S_N$ we have $u_i(s_1, ..., s_n) = u_{\pi(i)}(s_{\pi^{-1}(1)}, ..., s_{\pi^{-1}(n)})$ for all $i \in N$ and $(s_1, ..., s_n) \in A$.
            
            \begin{proof}
                Let $\Gamma$ be a VNM symmetric game, and let $i \in N$ and $(s_1, ..., s_n) \in A$. It follows that $u_{\pi(i)}(s_1, ..., s_n) = u_i(s_{\pi(1)}, ..., s_{\pi(n)})$ for all $\pi \in S_N$. Therefore $u_{i}(s_1, ..., s_n) = u_{\pi^{-1}(i)}(s_{\pi(1)}, ..., s_{\pi(n)})$ for all $\pi \in S_N$. Subbing in $\pi = \pi^{-1}$ we have $u_i(s_1, ..., s_n) = u_{\pi(i)}(s_{\pi^{-1}(1)}, ..., s_{\pi^{-1}(n)})$ for all $\pi \in S_N$. 
                
                The converse is the same in reverse.
            \end{proof}
        \end{proposition}
    
        \begin{proposition} 
            If a game is VNM symmetric then it has a player $n$-transitive and strategy trivial symmetry group.
            
            \begin{proof}
                Let $\Gamma$ be a VNM symmetric game, $j \in N$ and $M = \{(s_j, ..., s_j): s_j \in A_j\}$. Then for each $\pi \in S_N$ we have, $u_i(s) = u_i(s_1, ..., s_n) = u_{\pi(i)}(s_{\pi^{-1}(1)}, ..., s_{\pi^{-1}(n)}) = u_{M_{\pi}.i}(M_{\pi}.s)$ for all $s \in A$.
                
                Therefore $\{M_{\pi} \in \Aut(\Gamma): \pi \in S_N\}$ is a player $n$-transitive and strategy trivial symmetry group.
            \end{proof}
        \end{proposition}
        
        \begin{proposition} 
            If a game has a player $n$-transitive and strategy trivial symmetry group then it is isomorphic to a VNM symmetric game.
            
            \begin{proof}
                If $\Gamma$ is a game with a player $n$-transitive and strategy trivial symmetry group $G$ then there exists a matching of the strategy sets $M$ such that $G = \{M_{\pi}: \pi \in S_N\}$. If we relabel the elements of each $n$-tuple in $M$ as the same then the resulting game is VNM symmetric.
            \end{proof}
        \end{proposition}
        
        This shows that VNM symmetric games have the same structure as those with a player $n$-transitive and strategy trivial symmetry group. If our above conjecture is true then VNM symmetric games have the same structure as fully standard symmetric games.
        
        A game with a player $n$-transitive and strategy trivial symmetry group has the additional property that each player is playing each of their opponents symmetrically regardless of how all other opponents play. This means these games are fair regardless of the skill for each player.
         

    \section{Classifying a Game}
        While the notions of symmetric, standard symmetric and fully symmetric games give us various descriptive definitions of strategic fairness, they do not provide a constructive way to determine where a particular game lies in these classifications. Below we provide an overview of some ways to classify a given game.
        
        For a game to be fully symmetric it is necessary that there exist an automorphism for every player permutation. Therefore, to show that a game is not fully symmetric we need to show there does not exist an automorphism for some player permutation, and to show that a game is fully symmetric we need to show there exists an automorphism for each player permutation in some generating set of the player permutations.
        
        Since $S_2$ is its only transitive subgroup, it follows that a two player game is symmetric if and only if it is fully symmetric. Therefore to show whether or not a two player game is symmetric we can just show whether or not it is fully symmetric.
        
        Since $<(123)>$ is the least transitive subgroup of $S_3$ when ordered by $\subseteq$, for a three player game to be symmetric it is necessary that there exist an automorphism for each 3-cycle player permutation. Therefore, to show that a three player game is not symmetric we need to show there does not exist an automorphism for some 3-cycle player permutation, and to show that a three player game is symmetric we need to show there exists an automorphism for a 3-cycle player permutation.
        
        In general it is a little more difficult to show whether or not a game is symmetric. To show that a game is symmetric we need to show there exists an automorphism for every player permutation in some generating set of some transitive subgroup of the player permutations, and to show that a game is not symmetric we must either find $\Aut(\Gamma)$ and show that it is not player transitive or show that there does not exist an automorphism for some player permutation in each generating set of every transitive subgroup of the player permutations.
        
        We note that every $n$-cycle generates a transitive subgroup of $S_n$ but not all subgroups of $S_n$ contain an $n$-cycle for $n \geq 4$. For example, $\{e, (12)\circ (34), (13)\circ (24), (14) \circ (23)\}$ is a transitive subgroup of $S_4$ which does not contain a 4-cycle. 
        
        To show that a game is standard symmetric we need to find a matching of the strategy sets such that each player permutation in a transitive subgroup $H \subseteq S_N$ is an automorphism. If there does not exist $d_i$ pure strategies such that each player plays each of their own pure strategies in exactly one strategy and the utilities are all equal for each of these strategies, then we can conclude that the game is not standard symmetric.
        
                
    \section{Constructing Symmetric Games}
        We can construct a symmetric game by picking out game bijections that generate a player transitive subgroup $G$ of the bijections and for each $g \in G$ setting $u_i(s) = u_{g.i}(g.s)$ for all $i \in N$ and $s \in A$. 
        
        \begin{example}
            If we take the following as our subgroup of bijections.
            \[ G = \{ \bigl(e ; \bigl(\begin{smallmatrix} a & b \\ a & b \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} c & d \\ c & d \end{smallmatrix}\bigr)\bigr),
                                \bigl((12) ; \bigl(\begin{smallmatrix} a & b \\ c & d \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} c & d \\ a & b \end{smallmatrix}\bigr)\bigr) \}  \]
            
            Then $\bigl((12) ; \bigl(\begin{smallmatrix} d & c \\ b & a \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} d & c \\ a & b \end{smallmatrix}\bigr)\bigr) \in G$ requires that we have,
            \begin{align*}
                u_1(a, c) &= u_2(a, c) = \alpha   &   u_1(a, d) &= u_2(b, c) = \gamma \\
                u_1(b, c) &= u_2(a, d) = \beta   &   u_1(b, d) &= u_2(b, d) = \delta
            \end{align*} 
            
            Which results in the game given below.
            \begin{center}
                \begin{game}{2}{2}
                            \> $c$  \> $d$ \\
                    $a$   \> $\alpha, \alpha $  \> $\gamma, \beta $ \\
                    $b$   \> $\beta, \gamma $  \> $\delta, \delta $
                \end{game}
            \end{center}
            
            All $2 \times 2$ standard symmetric games are isomorphic to a game of this form, hence this essentially gives us a general form for the $2 \times 2$ standard symmetric games. We can pick out a particular $2 \times 2$ standard symmetric game by assigning values to $\alpha, \beta, \gamma$ and $\delta$.
        \end{example}
    
        To construct a game which is or is not fully symmetric we just need to choose bijections such that the subgroup they generate is or is not player $n$-transitive. 
        
        To construct a standard symmetric game we can pick out a matching of the strategy sets and player permutations in a transitive subgroup of $S_n$, then the game constructed from the induced game bijections is standard symmetric. Constructing a non-standard symmetric game is not so simple, we must actually construct the game and check that it is non-standard symmetric.
    
    \section{Examples of Symmetric Games}
        Below we provide some less trivial examples in each class of symmetric games which were generated using the construction method outlined in the last section.
    
        \begin{example}
            Fully standard symmetric three player game
            \begin{center}
                \begin{game}{2}{2}[$e$]
                          \> $c$      \> $d$ \\
                    $a$   \> $1,1,1$  \> $6,2,6$ \\
                    $b$   \> $2,6,6$  \> $5,5,3$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}[$f$]
                          \> $c$     \> $d$ \\
                    $a$   \> $6,6,2$ \> $3,5,5$ \\
                    $b$   \> $5,3,5$ \> $4,4,4$
                \end{game}
            \end{center}
            
            If we take the matching $\{(a, c, e), (b,d,f)\}$ of the strategy sets then the induced bijection for each player permutation is an automorphism. The set of these automorphisms forms a player transitive and strategy trivial symmetry group making it a fully standard symmetric game.
        \end{example}

        \begin{example}
            Non-fully standard symmetric three player game.
            \begin{center}
                \begin{game}{2}{2}[$e$]
                          \> $c$      \> $d$ \\
                    $a$   \> $1,1,1$  \> $3,5,7$ \\
                    $b$   \> $5,7,3$  \> $2,4,6$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}[$f$]
                          \> $c$     \> $d$ \\
                    $a$   \> $7,3,5$ \> $6,2,4$ \\
                    $b$   \> $4,6,2$ \> $8,8,8$
                \end{game}
            \end{center}
            
            
            \begin{align*}
                \Aut(\Gamma) = \{ \bigl(e ; \bigl(\begin{smallmatrix} a & b \\ a & b \end{smallmatrix}\bigr),& \bigl(\begin{smallmatrix} c & d \\ c & d \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} e & f \\ e & f \end{smallmatrix}\bigr)\bigr),
                               \bigl((123) ; \bigl(\begin{smallmatrix} a & b \\ c & d \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} c & d \\ e & f \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} e & f \\ a & b \end{smallmatrix}\bigr)\bigr), \\
                               &\bigl((321) ; \bigl(\begin{smallmatrix} a & b \\ e & f \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} c & d \\ a & b \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} e & f \\ c & d \end{smallmatrix}\bigr)\bigr),\} 
            \end{align*}
           
            Clearly $\Aut(\Gamma)$ is player transitive and strategy trivial making $\Gamma$ standard symmetric. Additionally, since there does not exist an automorphism for every permutation of the players, $\Gamma$ is non-fully standard symmetric.
            
        \end{example}
    
        \begin{example}
            Two non-fully non-standard symmetric four player games. The second is an example of a symmetric four player game which does not have an automorphism for each 4-cycle player permutation.
            
            \begin{center}
                \begin{game}{2}{2}[$(e,g)$]
                          \> $c$      \> $d$ \\
                    $a$   \> $1,2,3,4$  \> $4,1,3,2$ \\
                    $b$   \> $2,3,4,1$  \> $5,6,7,8$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}[$(f,g)$]
                          \> $c$     \> $d$ \\
                    $a$   \> $8,5,6,7$ \> $3,4,1,2$ \\
                    $b$   \> $7,8,5,6$ \> $6,7,8,5$
                \end{game}
                \\
                \begin{game}{2}{2}[$(e,h)$]
                          \> $c$      \> $d$ \\
                    $a$   \> $6,7,8,5$  \> $7,8,5,6$ \\
                    $b$   \> $3,4,1,2$  \> $8,5,6,7$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}[$(f,h)$]
                          \> $c$     \> $d$ \\
                    $a$   \> $5,6,7,8$ \> $2,3,4,1$ \\
                    $b$   \> $4,1,2,3$ \> $1,2,3,4$
                \end{game}
                
            \end{center}
            
           \[ \Aut(\Gamma) = < \bigl((1234) ; \bigl(\begin{smallmatrix} a & b \\ d & c \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} c & d \\ e & f \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} e & f \\ g & h \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} g & h \\ a & b \end{smallmatrix}\bigr)\bigr) >  \]
            
            
            \begin{center}
                \begin{game}{2}{2}[$(e,g)$]
                          \> $c$      \> $d$ \\
                    $a$   \> $1,1,2,2$  \> $3,4,4,3$ \\
                    $b$   \> $4,3,3,4$  \> $2,2,1,1$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}[$(f,g)$]
                          \> $c$      \> $d$ \\
                    $a$   \> $4,3,3,4$  \> $2,2,1,1$ \\
                    $b$   \> $1,1,2,2$  \> $3,4,4,3$
                \end{game}
                \\
                \begin{game}{2}{2}[$(e,h)$]
                          \> $c$     \> $d$ \\
                    $a$   \> $3,4,4,3$ \> $1,1,2,2$ \\
                    $b$   \> $2,2,1,1$ \> $4,3,3,4$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}[$(f,h)$]
                          \> $c$     \> $d$ \\
                    $a$   \> $2,2,1,1$ \> $4,3,3,4$ \\
                    $b$   \> $3,4,4,3$ \> $1,1,2,2$
                \end{game}
                
            \end{center}
            
            {\footnotesize 
            \begin{align*}
                \Aut(\Gamma) &= < \bigl((12)(34) ; \bigl(\begin{smallmatrix} a & b \\ d & c \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} c & d \\ a & b \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} e & f \\ h & g \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} g & h \\ e & f \end{smallmatrix}\bigr)\bigr), \\
                               &\bigl((13)(24) ; \bigl(\begin{smallmatrix} a & b \\ f & e \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} c & d \\ h & g \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} e & f \\ a & b \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} g & h \\ c & d \end{smallmatrix}\bigr)\bigr), 
                               \bigl((14)(23) ; \bigl(\begin{smallmatrix} a & b \\ h & g \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} c & d \\ f & e \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} e & f \\ c & d \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} g & h \\ a & b \end{smallmatrix}\bigr)\bigr) >
           \end{align*}
           }
           
           Clearly the game bijections generate games which are symmetric and not fully symmetric. Additionally, since there exist no strategy profiles such that the players' utility values are equal, the games are not standard symmetric. Hence, both games are non-fully non-standard symmetric.
        \end{example}
            
        \begin{example}
            Fully non-standard symmetric four player game.
            
            \begin{center}
                \begin{game}{2}{2}[$(e,g)$]
                          \> $c$      \> $d$ \\
                    $a$   \> $2,1,1,1$  \> $1,1,2,1$ \\
                    $b$   \> $2,1,1,1$  \> $1,1,1,2$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}[$(f,g)$]
                          \> $c$      \> $d$ \\
                    $a$   \> $1,1,1,2$  \> $1,1,2,1$ \\
                    $b$   \> $1,2,1,1$  \> $1,2,1,1$
                \end{game}
                \\
                \begin{game}{2}{2}[$(e,h)$]
                          \> $c$     \> $d$ \\
                    $a$   \> $1,2,1,1$ \> $1,2,1,1$ \\
                    $b$   \> $1,1,2,1$ \> $1,1,1,2$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}[$(f,h)$]
                          \> $c$     \> $d$ \\
                    $a$   \> $1,1,1,2$ \> $2,1,1,1$ \\
                    $b$   \> $1,1,2,1$ \> $2,1,1,1$
                \end{game}
                
            \end{center}
            
            {\small
                \[ \Aut(\Gamma) = < \bigl((1234) ; \bigl(\begin{smallmatrix} a & b \\ c & d \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} c & d \\ e & f \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} e & f \\ h & g \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} g & h \\ a & b \end{smallmatrix}\bigr)\bigr),
                               \bigl((12) ; \bigl(\begin{smallmatrix} a & b \\ c & d \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} c & d \\ a & b \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} e & f \\ e & f \end{smallmatrix}\bigr), \bigl(\begin{smallmatrix} g & h \\ h & g \end{smallmatrix}\bigr)\bigr) > \]
            }
            
            Clearly the game bijections generate a fully symmetric game, and since there exist no strategy profiles such that the players' utility values are equal the game is not standard symmetric.
        \end{example}
        
    \section{Incorrect Symmetric Game Classification}      
        Dasgupta and Maskin \cite{DMaskin} gave an incorrect definition of a symmetric game which we give below.
        
        \begin{definition} 
            A game $\Gamma$ is \textit{DM symmetric} if $A_i = A_j$ for all $i, j \in N$, and for each player permutation $\pi \in S_N$,  $u_i(s_1, ..., s_n) = u_{\pi(i)}(s_{\pi(1)}, ..., s_{\pi(n)})$ for all $i \in N$ and $(s_1, ..., s_n) \in A$.
        \end{definition}
        
        The problem with this definition is that it does not correctly permute the players and their strategies, for example for each player $i$, the right hand side does not have player $\pi(i)$ playing the strategy that player $i$ played.
    
        \begin{proposition} 
            A game is VNM symmetric if and only if for each player transposition $\pi \in S_N$, $u_i(s_1, ..., s_n) = u_{\pi(i)}(s_{\pi(1)}, ..., s_{\pi(n)})$ for all $i \in N$ and $(s_1, ..., s_n) \in A$.
            
            \begin{proof}
                Let $\Gamma$ be a VNM symmetric game and let $\pi \in S_N$ be a transposition. Then we have $u_i(s_1, ..., s_n) = u_{\pi(i)}(s_{\pi^{-1}(1)}, ..., s_{\pi^{-1}(n)})$ for all $i \in N$ and $(s_1, ..., s_n) \in A$. Therefore since $\pi = \pi^{-1}$ we have $u_i(s_1, ..., s_n) = u_{\pi(i)}(s_{\pi(1)}, ..., s_{\pi(n)})$ for all $i \in N$ and $(s_1, ..., s_n) \in A$.
                
                The converse is much the same except we note that the condition for a DM symmetric game is met for all player transpositions since it is met for all player permutations.
            \end{proof}
        \end{proposition}
            
        This shows that if a game is DM symmetric then it is VNM symmetric. Furthermore, since each element of $S_2$ is its own inverse, it follows that the two notions are equivalent for two player games. 
        
        Below we give an example of a three player VNM symmetric game which is not DM symmetric to show that the classifications are not equivalent for games with at least three players.
    
        \begin{example}
            Three Player Symmetric Game
            \begin{center}
                \begin{game}{2}{2}[$a$]
                          \> $a$      \> $b$ \\
                    $a$   \> $1,1,1$  \> $2,3,2$ \\
                    $b$   \> $3,2,2$  \> $4,4,5$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}[$b$]
                          \> $a$     \> $b$ \\
                    $a$   \> $2,2,3$ \> $5,4,4$ \\
                    $b$   \> $4,5,4$ \> $6,6,6$
                \end{game}
            \end{center}
            
            If we take $\pi = \bigl(\begin{smallmatrix} 1 & 2 & 3 \\ 2 & 3 & 1 \end{smallmatrix}\bigr) \in S_3$ and $s = (b, a, a) \in A$, we see that
            \begin{align*}
                3 = u_1(b, a, a) &= u_1(s_1, s_2, s_3) \\
                & \neq u_{\pi(1)}(s_{\pi(1)}, s_{\pi(2)}, s_{\pi(3)}) = u_2(s_2, s_3, s_1) = u_2(a, a, b) = 2
            \end{align*} 
            
            It should be fairly obvious that if we are mapping player 1 to player 2 and player 1 is playing $b$ then we want the mapped strategy to have player 2 playing $b$, but this is not what is required for a DM symmetric game.
        \end{example}
            
        We could restrict the condition for a DM symmetric game to just player transpositions, however transpositions are not closed under composition so the composition of automorphisms would not necessarily meet this condition, making it fairly undesirable. 
        
    \section{Equilibria in Symmetric Games}
        We now present two famous theorems without proof relating to the existence of Nash equilibria in symmetric games. Our first result from Nash \cite{NashNCG} gives the existence of a symmetric Nash equilibrium.
        
        \begin{theorem}
            Every symmetric game has a symmetric Nash equilibrium.
        \end{theorem}
        
        Our second result by Cheng et al. \cite{CRVWSym} gives the existence of a pure strategy Nash equilibrium for fully standard symmetric $2 \times ... \times 2$ games.
        
        \begin{theorem} 
            Every fully standard symmetric $2\times ... \times 2$ game has a pure strategy Nash equilibrium.
        \end{theorem}
        
        Cheng \textit{et al.} \cite{CRVWSym} noted that rock, paper, scissors is an example of a fully standard symmetric $3 \times 3$ game with no pure strategy Nash equilibria, and indirectly that matching pennies is an example of a fully non-standard symmetric $2\times 2$ game with no pure strategy Nash equilibria. 
        
        \begin{example}
            Rock, paper, scissors.
            
            \begin{center}
                \begin{game}{3}{3}
                          \> $R$      \> $P$      \> $S$\\
                    $R$   \> $0,0$  \> $0,1$  \> $1,0$ \\
                    $P$   \> $1,0$  \> $0,0$  \> $0,1$ \\
                    $S$   \> $0,1$  \> $1,0$  \> $0,0$
                \end{game}
            \end{center}
        \end{example}
        
        However they only considered fully standard symmetric games as symmetric, consequently not considering whether the result might hold for non-fully standard symmetric games. We provide an example below to show that this is not the case.
        
        \begin{example}
            A non-fully standard symmetric $2 \times 2 \times 2$ game with no pure strategy Nash equilibria.
            
            \begin{center}
                \begin{game}{2}{2}[$e$]
                          \> $c$      \> $d$ \\
                    $a$   \> $2,2,2$  \> $6,4,5$ \\
                    $b$   \> $4,5,6$  \> $8,1,7$
                \end{game}
                \hspace*{10mm} 
                \begin{game}{2}{2}[$f$]
                          \> $c$     \> $d$ \\
                    $a$   \> $5,6,4$ \> $7,8,1$ \\
                    $b$   \> $1,7,8$ \> $3,3,3$
                \end{game}
            \end{center}
        \end{example}
        
